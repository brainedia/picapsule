# PiTimeCapsule

Easily setup a [Raspberry Pi](https://www.raspberrypi.org) (running [Raspbian](https://www.raspberrypi.org/downloads/raspbian/)) as a [NAS](https://de.wikipedia.org/wiki/Network_Attached_Storage) for usage with [TimeMachine](https://en.wikipedia.org/wiki/Time_Machine_(macOS)).

The first solution building a TimeMachine-Server upon a [RAID1](https://en.wikipedia.org/wiki/Standard_RAID_levels#RAID_1) NAS.


# References
1.  https://www.howtogeek.com/276468/how-to-use-a-raspberry-pi-as-a-networked-time-machine-drive-for-your-mac/
2.  https://www.stewright.me/2017/08/create-raid-volume-raspberry-pi/
3.  https://sammit.net/how-to-make-a-raspberry-pi-nas-using-samba-hfs/
4.  http://www.andadapt.com/raspberry-pi-raspbian-hfs-afp-and-time-machine/
5.  https://blog.alexellis.io/hardened-raspberry-pi-nas/
6.  