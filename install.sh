#!/bin/bash

sudo apt-get update
sudo apt-get upgrade

./install_hfs_support.sh
./install_netatalk.sh

sudo mkdir -p /media/ExternalHD/

sudo apt-get install -y mdadm

sudo fsck.hfsplus -f /dev/sda2
sudo fsck.hfsplus -f /dev/sdb2

sudo mdadm --create /dev/md0 --level=mirror --raid-devices=2 /dev/sda2 /dev/sdb2
sudo mkfs.hfsplus /dev/md0
sudo mount -t hfsplus -o force,rw /dev/md0 /media/ExternalHD
sudo mdadm --detail --scan | sudo tee -a /etc/mdadm/mdadm.conf